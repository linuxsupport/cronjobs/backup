job "${PREFIX}_backup" {
  datacenters = ["meyrin"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_backup" {
    driver = "docker"

    config {
      args = [ ${ARGS} ]
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/backup/backup:${CI_COMMIT_REF_NAME}"
      force_pull = ${FORCE_PULL}
      logging {
        config {
          tag = "${PREFIX}_backup"
        }
      }
      volumes = [
        "/opt/tivoli/tsm/client/ba/bin/dsm.sys:/opt/tivoli/tsm/client/ba/bin/dsm.sys",
        "/opt/tivoli/tsm/client/ba/bin/dsm.opt:/opt/tivoli/tsm/client/ba/bin/dsm.opt",
        "/opt/tivoli/tsm/client/ba/bin/backup.excl:/opt/tivoli/tsm/client/ba/bin/backup.excl",
        "/etc/adsm/TSM.PWD:/etc/adsm/TSM.PWD",
        "/mnt/data1:/mnt/data1",
        "/mnt/data2:/mnt/data2",
      ]
    }

    resources {
      cpu = 1000 # Mhz
      memory = 1024 # MB

      network {
        mbits = 100
      }
    }

  }
}
