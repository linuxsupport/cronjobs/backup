# WARNING

This job is no longer working because the newer version of the TSM client can't seem to run
inside a docker container, and we can't run the old versions any longer.

Backups are now performed as regular old cronjobs, configured here:

https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/-/blob/feb86a32/code/manifests/adm.pp#L123-139

The `.gitlab-ci.yml` of this repo has been edited to "deactivate" this job.
